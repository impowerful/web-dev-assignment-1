"uses strict";

/**
 * @description Creates a new HTML element takng input string tagname, string content, string parent
 * 
 * @param {string} tagname - tag name of the element to create
 * @param {string} content - text inside the HTML element
 * @param {string} parent - parent to append the tag name to
 * @returns - new HTML element 
 */
function addHTMLElement(tagname, content, parent) {
    let new_node = document.createElement(tagname);
    new_node.textContent=content;
    let par = document.getElementById(parent);
    par.append(new_node);
    return new_node;
}

/**
 * @description changes the text color of the tagname taking input of string tagname, hexcode textColor
 * 
 * @param {string} tagname 
 * @param {hexcode} textColor 
 */
function changeTextColor(tagname, textColor) {
    let element = document.querySelectorAll(tagname);
    for (let i = 0; i < element.length; ++i){
        element[i].style.color = textColor;
    }
}

/**
 * @description changes the background color of the tagname taking input of string tagname, hexcode backgroundColor
 * 
 * @param {string} tagname 
 * @param {hexcode} backgroundColor 
 */
function changeBackground(tagname, backgroundColor) {
    let element = document.querySelectorAll(tagname);
    for (let i = 0; i < element.length; ++i){
        element[i].style.backgroundColor = backgroundColor;
    }
}

/**
 * @description changes the table width of the tagname taking input of string tagname, number percentage
 * 
 * @param {string} tagname 
 * @param {number} percentage 
 */
function changeTableWidth(tagname, percentage) {
    let element = document.querySelectorAll(tagname);
    for (let i = 0; i < element.length; ++i){
        element[i].style.width = percentage + "%";
    }
}

/**
 * @description changes the border color of the tagname taking input of string tagname, hexcode borderColor
 * 
 * @param {string} tagname 
 * @param {hexcode} borderColor 
 */
function changeBorderColor(tagname, borderColor) {
    let element = document.querySelectorAll(tagname);
    for (let i = 0; i < element.length; ++i){
        element[i].style.borderColor = borderColor;
    }
}

/**
 * @description changes the border width of the tagname taking input of string tagname, hexcode textColor
 * 
 * @param {string} tagname 
 * @param {number} pixel
 */
function changeBorderWidth(tagname, pixel) {
    let element = document.querySelectorAll(tagname);
    for (let i = 0; i < element.length; ++i){
        element[i].style.borderWidth = pixel + "px";
    }
}

/**
 * @description Initializing a variable and returning the value of the id
 * @returns - returning value of the id row_count
 */
 function getRowCount() {
    let row_count = document.getElementById("row_count").value;
    return row_count;
}

/**
 * @description Initializing a variable and returning the value of the id
 * @returns - returning value of the id column_count
 */
function getColumnCount() {
    let column_count = document.getElementById("column_count").value;
    return column_count;
}

/**
 * @description Initializing a variable and returning the value of the id
 * @returns - returning value of the id table_width
 */
function getTableWidth() {
    let table_width = document.getElementById("table_width").value;
    return table_width;
}

/**
 * @description Initializing a variable and returning the value of the id
 * @returns - returning value of the id text_color
 */
function getTextColor() {
    let text_color = document.getElementById("text_color").value;
    return text_color;
}

/**
 * @description Initializing a variable and returning the value of the id
 * @returns - returning value of the id background_color
 */
function getBackgroundColor() {
    let background_color = document.getElementById("background_color").value;
    return background_color;
}

/**
 * @description Initializing a variable and returning the value of the id
 * @returns - returning value of the id border_width
 */
function getBorderWidth() {
    let border_width = document.getElementById("border_width").value;
    return border_width;
}

/**
 * @description Initializing a variable and returning the value of the id
 * @returns - returning value of the id border_color
 */
function getBorderColor() {
    let border_color = document.getElementById("border_color").value;
    return border_color;
}