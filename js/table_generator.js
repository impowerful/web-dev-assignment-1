"uses strict";

/**
 * @description loading the DOM and then adding all the eventlisteners and initializing the variables 
 */
window.addEventListener('DOMContentLoaded', function () {
    console.log('DOM has been fully loaded and parsed able to proceed');

    let border_color = document.getElementById("border_color");
    border_color.addEventListener("change", generateTable);

    let table_width = document.getElementById("table_width");
    table_width.addEventListener("change", generateTable);

    let row_count = document.getElementById("row_count");
    row_count.addEventListener("keyup", generateTable);

    let column_count = document.getElementById("column_count");
    column_count.addEventListener("keyup", generateTable);

    let background_color = document.getElementById("background_color");
    background_color.addEventListener("change", generateTable);

    let text_color = document.getElementById("text_color");
    text_color.addEventListener("change", generateTable);

    let border_width = document.getElementById("border_width");
    border_width.addEventListener("keyup", generateTable);


});

/**
 * @description Dynamically generates table with all the functions get and addHTMLElement 
 * and then calls all the change method to change css alongside witg generating text area
 */
function generateTable() {

    if(document.getElementById("table-created") !=null) {
        document.getElementById("table-created").remove();
    }

    let new_table = addHTMLElement("table", null, "table-render-space");
    new_table.id = "table-created";

    let row_count = getRowCount();
    let column_count = getColumnCount();

    for (let i = 0; i < row_count; ++i) {

        let currentTableRow = addHTMLElement("tr", null, "table-created");
        let tableRow = "tr" + i;

        currentTableRow.id = tableRow;

        for(let j = 0; j < column_count; ++j) {
            addHTMLElement("td", "cell" + i + j, tableRow);
        }
    }

    changeTableWidth("table", getTableWidth());
    changeTextColor("table", getTextColor());
    changeBackground("td", getBackgroundColor());
    changeBorderWidth("td", getBorderWidth());
    changeBorderColor("td", getBorderColor());

    generateHTML();
}

/**
 * @description Dynamically generate a HTML element textarea and checking if there 
 * is one if yes removing and creating a new alsongside with styling the printout
 */
function generateHTML() {
    if (document.querySelector("textarea") != null) {
        document.querySelector("textarea").remove();
    }
    addHTMLElement("textarea", null, "table-html-space");
    document.querySelector("textarea").readOnly=true;

    let textArea = "<table>\n";
    for (let i = 0; i < getRowCount(); ++i) {
        textArea += "    <tr>\n";
        for (let j = 0; j < getColumnCount(); ++j) {
            textArea += "      <td>cell" + i + j + "</td>\n";
        }
        textArea += "    </tr>\n";
    }
    textArea += "</table>";

    document.querySelector("textarea").value = textArea;
}